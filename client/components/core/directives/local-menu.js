import angular from 'angular';

localMenu.$inject = [];

function localMenu() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: require('../layouts/local-menu.html'),
    bindToController: {},
    controller: localMenuCtrl,
    controllerAs: 'lm',
  }
};

localMenuCtrl.$inject = [];

function localMenuCtrl() {
  this.test2 = () => {
    console.log("TEST 2");
  }
  console.log("Join - Local Menu");
};

export default localMenu;
