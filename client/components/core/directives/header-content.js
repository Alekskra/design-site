import angular from 'angular';

headerContentDirective.$inject = [];

function headerContentDirective() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: require('../layouts/header-content.html'),
    bindToController: {},
    controller: headerContentCtrl,
    controllerAs: 'hc',
  }
};


headerContentCtrl.$inject = [];

function headerContentCtrl() {
  console.log("Join - Header Content");
}

export default headerContentDirective;
