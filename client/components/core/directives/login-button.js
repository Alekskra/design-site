import angular from 'angular';

logBtnDirective.$inject = [];

function logBtnDirective() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    bindToController: {},
    template: require('../layouts/login-button.html'),
    controller: logBtnCtrl,
    controllerAs: 'logbtn',
  }
}

logBtnCtrl.$inject = [];

function logBtnCtrl() {
  console.log(this.processing)

}

export default logBtnDirective;
