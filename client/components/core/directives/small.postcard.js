import angular from 'angular';

smallPostcard.$inject = [];

function smallPostcard() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: require('../layouts/small.postcard.html'),
    bindToController: {},
    controller: smallPostcardCtrl,
    controllerAs: 'spc',
  }
};

smallPostcardCtrl.$inject = [];

function smallPostcardCtrl() {
  console.log("Join - small Post Card");
}


export default smallPostcard;
