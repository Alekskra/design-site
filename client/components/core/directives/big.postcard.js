import angular from 'angular';

bigPostcard.$inject = [];

function bigPostcard() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: require('../layouts/big.postcard.html'),
    bindToController: {},
    controller: bigPostcardCtrl,
    controllerAs: 'bpc',
  }
};

bigPostcardCtrl.$inject = [];

function bigPostcardCtrl() {
  console.log("Join - big Post Card");
}


export default bigPostcard;
