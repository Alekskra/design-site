import angular from 'angular';

localButtonDirective.$inject = [];

function localButtonDirective() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      dsClick: '&',
    },
    template: require('../layouts/local-button.html'),
    bindToController: {},
    controller: localButtonCtrl,
    controllerAs: 'lb',
  }
};

localButtonCtrl.$inject = [];

function localButtonCtrl() {

};


export default localButtonDirective;
