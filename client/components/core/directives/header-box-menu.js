import angular from 'angular';

headerBoxMenu.$inject = [];

function headerBoxMenu() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: require('../layouts/header-box-menu.html'),
    bindToController: {},
    controller: headerBoxCtrl,
    controllerAs: 'hb',
  }
};

headerBoxCtrl.$inject = [];

function headerBoxCtrl() {
  console.log("Join - Header Box Menu");
};

export default headerBoxMenu;
