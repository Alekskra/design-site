import angular from 'angular';

globalMenuDirective.$inject = [];

function globalMenuDirective() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: require('../layouts/global-menu.html'),
    bindToController: {},
    controller: globalMenuCtrl,
    controllerAs: 'gm',
  }
};

globalMenuCtrl.$inject = [];

function globalMenuCtrl() {
  console.log("Join - Global Menu")
}

export default globalMenuDirective;
