import './resources/scss/main';
import angular from 'angular';

import '../home';

import logBtnDirective from './directives/login-button';
import globalMenuDirective from './directives/global-menu';
import headerContentDirective from './directives/header-content';
import localMenu from './directives/local-menu';
import localButtonDirective from './directives/local-button';

angular.module('app.core', [
    'ui.router',
    'app.home',
  ])
  .config(configureModule)
  .directive('globalMenu', globalMenuDirective)
  .directive('headerContent', headerContentDirective)
  .directive('localMenu', localMenu)
  .directive('logButton', logBtnDirective)
  .directive('lButton', localButtonDirective)

configureModule.$inject = ['$stateProvider'];

function configureModule($stateProvider) {
  $stateProvider
    .state('core', {
      template: require('./layouts/index.html'),
      abstract: true,
    });
};
