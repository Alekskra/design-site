import angular from 'angular';

endedCompetitionsBlock.$inject = [];
function endedCompetitionsBlock (){
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: require('../layouts/endedCompetitions-block.html'),
    bindToController: {},
    controller: endedCompBlockCtrl,
    controllerAs: 'endCompB',
  }
};


endedCompBlockCtrl.$inject = [];

function endedCompBlockCtrl(){
  console.log("Join - Ended Competitions Block");
};

export default endedCompetitionsBlock;
