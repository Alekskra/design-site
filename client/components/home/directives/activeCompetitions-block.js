import angular from 'angular';

activeBlockDirective.$inject = [];

function activeBlockDirective() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: require('../layouts/activeCompetitions-block.html'),
    bindToController: {},
    controller: activeBlockCtrl,
    controllerAs: 'actCompB',
  }
};

activeBlockCtrl.$inject = [];

function activeBlockCtrl() {
  console.log("Join - Active Competitions");
}

export default activeBlockDirective;
