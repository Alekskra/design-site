import './resources/scss/home.style';


import angular from 'angular';

import headerBoxMenu from '../core/directives/header-box-menu';
import activeBlockDirective from './directives/activeCompetitions-block';
import endedBlockDirective from './directives/endedCompetitions-block';
import smallPostcard from '../core/directives/small.postcard';
import bigPostcard from '../core/directives/big.postcard';
import localButtonDirective from '../core/directives/local-button';
import logBtnDirective from '../core/directives/login-button';

angular.module('app.home', ['app.core'])
  .config(configureModule)
  .directive('headerBoxMenu', headerBoxMenu)
  .directive('activeBlock', activeBlockDirective)
  .directive('smallPostcard', smallPostcard)
  .directive('endedBlock', endedBlockDirective)
  .directive('bigPostcard', bigPostcard)
  .directive('loginButton', logBtnDirective)

configureModule.$inject = ['$stateProvider'];

function configureModule($stateProvider) {
  $stateProvider
    .state('core.home', {
      url: '/',
      template: require('./layouts/index.html'),
    });
}
