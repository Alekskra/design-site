import 'bootstrap';
import 'bootstrap-material-design';
import angular from 'angular';
import 'angular-ui-router';
require('font-awesome/scss/font-awesome.scss');

import './components/core';
import './components/session';

angular.module('DesignStudio', [
  // application dependencies
  'app.core',
  'app.session',
]).config(configureModule);

configureModule.$inject = ['$urlRouterProvider'];

function configureModule($urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
}
