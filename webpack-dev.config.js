const webpack = require('webpack');

const devConfig = require('./make-webpack-config')({
  devServer: {
    contentBase: './build',
  },
  module: {
    postLoaders: [],
  },

  devtool: 'eval'
})

devConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

module.exports = devConfig;
